#!/usr/bin/python3
import os
import shutil
import xml.etree.ElementTree
import sys

# recupere l archive xml pour base de travail
shutil.copyfile("repertoire_scrute_v0.xml", "repertoires_scrute_v0.xml")
# Open original file
et = xml.etree.ElementTree.parse('repertoires_scrute_v0.xml')

def exist_id():
	#a partir du xml defini en variable globale
	expertise = et.getElementsByTagName("theme")

	#on parcours le squelette xml definit
	for skill in expertise:
		#on recupere les paramètres
		id_encours = skill.getAttribute("id")
		print("id_encours")
	return true

def undeplus(tag_encours,repertoire,fichier,tag):
	# Append new tag: <action repertoire='repertoire' autre_tag='tag'>fichier</action>
	new_tag=tag_encours
	#on verifie que l objet existe - si il n existe pas on l initialise - sinon on garde l objet passe en parametre:objectif ne pas generer plusieurs balises xml identique
	if  len(tag_encours) <= 0 :
		new_tag = xml.etree.ElementTree.SubElement(et.getroot(), 'theme')
		new_tag.attrib['id'] = repertoire # must be str; cannot be an int
	new_tag.attrib['nom'] = tag
	new_tag_niv2 = xml.etree.ElementTree.SubElement(new_tag, 'type')
	new_tag_niv2.attrib['nom'] = fichier

	# Write back to file
	et.write('repertoires_scrute_v0.xml')
	return new_tag

def scrute(repertoire):
	# retourne les fichiers d un repertoire
	return next(os.walk(repertoire))[2]

def rep_en_cours():
	#retourne le repertoire en cours
	return os.getcwd() # todo rajouter code erreur si pas repertoire et envoie liste vide

def main(arguments):
	print("Commence ...")
	print(arguments)
	#pour tout les fichiers (ligne) dans le repertoire en cours (scrute)
	arguments=arguments[1:len(arguments)]
	for rep in arguments:
		print(rep)
		tagencours=""
		for ligne in scrute(rep):
			rep_cible=rep_en_cours()		
			rep_cible_complet=rep_cible+"/"+rep
			#on dissocie le premier tour de boucle objet xml inexistant
			if len(tagencours) > 0 :
				undeplus(tagencours,rep_cible_complet,ligne,"na")
			else :
				tagcache=undeplus(tagencours,rep_cible_complet,ligne,"na")
				tagencours=tagcache

if __name__ == "__main__":
	#lance la scrutation sur les repertoires entres en arguments
	main(sys.argv);
