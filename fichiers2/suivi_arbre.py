#La version 0.x dans un premier temps a pour but de deplacer les fichiers present d'un repertoire source (non trié) vers un repertoire cible (tague dans le fichiers arbre1.xml) / dans un deuxième generé les tags quand un fichier du repertoire source n'est pas tague dans arbre1.xml a partir de critere divers comme le nom du fichier, son extension, son contenu / gestion de tag croise - optimisation de tris - tris semantique est l'objectif pour la version 0.9 / pour les version anterieur test de structure de donnees souples et mise en place de constructeur et fonctions metiers de base ( cherche, generer nouvea tag pour repertoire existant, generer repertoire si tag incompatible avec repertoire existant dans arbre1.xml )
#auteur LMG
#license GPL V3 : https://www.gaiac.eu/gpl.txt
#version: 0.3
#!/usr/bin/python3
from xml.dom.minidom import parse
import xml.dom.minidom
import argparse
import sys
import os


#variable globale pour version 0.x travaillant uniquement sur arbre1.xml
chemin_encours = os.path.abspath(os.path.dirname(__file__))
doc=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")


class outils(object):
	def etpuis(rien,arguments):
		if arguments in motclef:
			print ( arguments )

	#mets à jour le xml avec l'objet skilla
	def maj(rien,skilla,enplus):
		#ouvre le fichier xml pour maj
		chemin_encours = os.path.abspath(os.path.dirname(__file__))
		doca=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")
		#créé une branche avec le contenu
		x = doc.createElement('type nom="'+enplus+'"')		
		#x = doc.createElement(enplus)
		#genere la branche dans le squelette du xml
		skilla.appendChild(x)
		#maj le xml
		with open("arbre1.xml", "w") as f:
		    doc.writexml(f)
		return

	#deplace element vers id_encours
	def deplacer(rien,element,id_encours):
		commande= 'mv ' + element + ' ' + id_encours
		#print pour version test
		print( commande )
		return

#fonction principale.Scrute le fichier arbre1.xml. Rajoute tag si besoin dans le xml ou deaplce le fichier cible vers le repertoire associé au tag
def scrut(fonction="",arguments=""):
	i=0
	node= outils()

	#a partir du xml defini en variable globale
	expertise = doc.getElementsByTagName("theme")

	#on parcours le squelette xml definit
	for skill in expertise:
		element_encours="e"
		passe=False
		#on recupere les paramètres
		id_encours = skill.getAttribute("id")
		niv2 = skill.getElementsByTagName("type")
		#pour chaque niveau du squelette xml
		for skill2 in niv2:
			nom_encours = skill2.getAttribute("nom")
			#on gère chaque elements passés en argument pour un niveau
			for element in arguments:
				#sauvegarde pour suivi dans algo
				element_encours=element
				#si un des arguments est dans le niveau du xml en cours
				if nom_encours in element:
					#on note qu il a ete trouvé:flag a 1
					passe=True
					#compteur du nombres d elements trouvé
					i=i+1
					z=z+1
					#print(element_encours)
					#mise à 0 de la sauvegarde si trouve
					element_encours=""
					#si tag fichier on maj
					if fonction == "add":
						print("a",i,nom_encours,passe)
					print(passe)
					#prepa argument pour envoie vers fonction deplacé
					if fonction == "mv":
						node.deplacer(element,id_encours)
				#fichiers non tagable:rajout tag dans type en cours
				if not passe:
					print("b",i,element_encours,passe)
					node.maj(skill,element_encours)
				#remise à 0 du flag pour recommencer boucle
				passe=False
			#suivis par branche du nombre de trouve:pour debuggage
			z=0
		#possibilite de partir sur autre  branche du xml
		niv2 = skill.getElementsByTagName("niv")

		for skill2 in niv2:
			bloc='bloc'

def main(arguments):
	#gestion arguments
	#description
	parser = argparse.ArgumentParser(description='gestion indexation fichiers')
	#backdoor
	#tous les arguments en paramètre
	parser.add_argument('pfiou', metavar='N', type=str, nargs='+', help='an integer for the accumulator')
#	parser.add_argument('--add', dest='accumulate', action='store_const', const=scrut("add",arguments), default=max, help='sum the integers (default: find the max)')
#	parser.add_argument('--mv', dest='accumulate', action='store_const', const=scrut("mv",arguments), default=max, help='sum the integers (default: find the max)')

#	parser.add_argument('integers', metavar='N', type=str, nargs='+', help='an integer for the accumulator')
	#gestion argument add:considere les fichiers en cours comme devant etre tag dans xml
	parser.add_argument("--add", help="index les tags des repertoires")
	#argument mv deaplce les fichiers du repertoire courant vers tag(si existe pas, induit un tag selon plusieurs critères dont repertoire en cours et nom du fichier en cours
	parser.add_argument("--mv", help="deplace selon l index")
	args = parser.parse_args()
	#traitement des arguments
	if args.add:
		encours=args.pfiou
		scrut("add",encours)

	if args.mv:
		encours=args.pfiou
		scrut ("mv",encours)


if __name__ == "__main__":
	main(sys.argv);

