#La version 0.x dans un premier temps a pour but de deplacer les fichiers present d'un repertoire source (non trié) vers un repertoire cible (tague dans le fichiers arbre1.xml) / dans un deuxième generé les tags quand un fichier du repertoire source n'est pas tague dans arbre1.xml a partir de critere divers comme le nom du fichier, son extension, son contenu / gestion de tag croise - optimisation de tris - tris semantique est l'objectif pour la version 0.9 / pour les version anterieur test de structure de donnees souples et mise en place de constructeur et fonctions metiers de base ( cherche, generer nouvea tag pour repertoire existant, generer repertoire si tag incompatible avec repertoire existant dans arbre1.xml )
#auteur LMG
#license GPL V3 : https://www.gaiac.eu/gpl.txt
#version: 0.3
#!/usr/bin/python3
from xml.dom.minidom import parse
import xml.dom.minidom
import argparse
import sys
import os


#variable globale pour version 0.x travaillant uniquement sur arbre1.xml
chemin_encours = os.path.abspath(os.path.dirname(__file__))
doc=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")
docx=xml.dom.minidom.parse(chemin_encours+"/basexml.xml")

def recherche_internet(motclef1,motclef2):
	print("recherche effectuée")
	return False

def creat_def_manu(arg, mot, list_arg):
	print(arg)
	print(mot)
	print(list_arg)

	#ouvre le fichier xml pour maj
	chemin_encours = os.path.abspath(os.path.dirname(__file__))
	doca=xml.dom.minidom.parse(chemin_encours+"/dic1.xml")
	#créé une branche avec le contenu
	id_encours = skilla.getAttribute("id")
	print("def en cours",id_encours)
	for mot in list_arg:
		x = doc.createElement('type nom="'+enplus+'"')
		#genere la branche dans le squelette duobject xml
		skilla.appendChild(x)
	#maj le xml
	with open("dic1.xml", "w") as f:
 	  	doc.writexml(f)

	return True

#chargement avant utilisation des fonctions seman
def chargement_lire():
	accord  = ["je", "tu", "il", "ils", "elle", "elles", "nous", "vous"]
	auxiliaire = ["etre","avoir"]
	grammaire = [""]
	adjectif = ["beau","peur","puissant","faible","fort"]
	nom = ["fleur","voiture","couleur","avion","film","astronomie","journal","petal"]
	verbe = ["courir"]

#decortique une definition
def decortique(file_name):
	compte_rendu = [""]
	onprend = False
	texte = "tu vois pourquoi"
	for mot in texte:
		if mot in accord:
			texte.replace(mot,"")
			onprend = False

		if mot in grammaire:
			onprend = False

		if mot in adjectif:
			onprend = True

		if mot in nom:
			onprend = True

		if infinitif(mot) in verbe:
			onprend = True

		if onprend :
			compte_rendu.append[mot]

		onprend = False
	return compte_rendu

#implemente un dictionnaire au format xml a partir d une definition trouve sur plusieurs support
def create_def(atome):
	try:
		#commande = "wget"
		#os.system(commande)
		#1premiere tentative de definition
		print("creation definition en cours")
		site_url = "https://fr.wikipedia.org/wiki/" + atome
		file_name = wget.download(site_url)
		#tentative d atomiser la definition mot par mot
		liste_mot_significatif_dic = decortique(file_name)
		print("creation definition effectuée :" + file_name)
		#2deuxieme tentative de definition
		#3troisieme tentative de definition
		return liste_mot_significatif_dic
	except:
		print("connection refusee")

#trouve la definition dans un dictionnaire au format xml du mot objectif ( pour la creation du dic voir fonction create_def
def find_mot_dic_xml(objectif):
	# on initialise le resultat par le mot lui meme
	liste_mot_significatif_dic = [objectif]
	#a partir du xml defini en variable globale
	expertise = dic.getElementsByTagName("theme")

	#on parcours le squelette xml definit : theme > id> type > nom
	for skill in expertise:
		#on recupere les paramètres
		#le mot
		id_encours = skill.getAttribute("id")
		# sa definition
		niv2 = skill.getElementsByTagName("type")
		# le mot est dans le dictionnaire
		if id_encours == objectif:
			for skill2 in niv2:
				nom_encours = skill2.getAttribute("nom")
				liste_mot_significatif_dic.append(nom_encours)

	# le mot n est pas dans le dictionnaire
	if len(liste_mot_significatif_dic) == 1 :
		nouveau = create_def(objectif)
	return liste_mot_significatif_dic


#parmis tous les mots definissant le mot objectif - choisir le principal le definissant
def find_first(objectif):
	# initialisation du resultat en retournant une liste de mots definissant l objectif
	approche = find_mot_dic_xml(objectif)

	# on recupere soit les elements principaux ( >1 est dans le dic ) soit le mot lui meme si pas dans le dicitionnaire
	if len(approche) > 1 :
		premiere_approche = approche[1]
	else:
		premiere_approche = approche[0]
	resultat_premier = premiere_approche
	return resultat_premier

#objectif est un mot passe en parametre on doit retourner un seul mot secondaire pour le definir
def solo(objectif):
	idee = find_first(objectif)
	#si le resultat est l objectif alors rien trouve
	if idee == objectif :
		idee = objectif
	print("resultat : " + objectif + ":" + idee + ":")
	return idee

#force la creation automatique d un tag theme ( par vocabulaire ou recherche internet ) en fonction du nom du fichier
def creat (fonction="",arguments=""):
	# message5.find("mond")
	# .replace("L", "pizza")
	# [1:9]
	
	print("creation theme en cours")

	#pour chaque nom de fichiers
	for molecule in arguments:
		#coupe le nom du fichier mot par mot
		#separateur des mots par defaut est l espace - on pars du premier
		deb = molecule.find(" ")
		repartie = False
		#1si le nom de fichier fait plus de un mot on cherche le mot principal
		#NB: remplacer par split pour le traitement des donnees
		while deb >= 0:
			#atomiser
			#la taille du nouveau nom de fichier une fois le mot precedent traite
			fin = len(molecule)

			print("molecule:" + molecule + ":")
			#for atome in molecule:
			#	print(atome)
			index = molecule.find(" ")
			# memoire pour la fin du traitement
			indexf = index
			if index < 0:
				indexf = fin 
			print(":" + molecule[0:indexf] + ":")
			deb = index
			molecule = molecule[deb+1:fin]
			repartie = True
		else:
			print("solo")
			#2 si le nom de fichier fait un mot il est deja atomise
			if not repartie :
				atome = molecule
			#on lance la recherche pour trouver un mot significatif de la definition du mot principal resortant du nom de fichier
			solo(atome)
	return True

#genere un arbe a partir de 0 - l arbre prend le nom de la variable xml
def gen(xml,arguments):
	cible = "ok"
	args = "rep"
	xmlcible = arguments[0]

	#ouvre le fichier xml pour maj
	mon_chemin_encours = os.path.abspath(os.path.dirname(__file__))

	if len(arguments) > 2 :
		xmlcible = arguments[0]

		liste_files = arguments[1:len(arguments)]

		print ("Gen @" + ", cible :" + cible + ", args :" + args + ", xml :" + xmlcible)
	else :
		return

	#possible
	roota = docx.getElementsByTagName("classement")
	skill = roota[0]

	skillN = docx.createElement("theme")
	skill.appendChild(skillN)
	skillN.setAttribute("id",mon_chemin_encours)

	for files in liste_files:
		if ( files[len(files)-1] == ":" ) :
			skillN = docx.createElement("theme")
			skill.appendChild(skillN)
			skillN.setAttribute("id",files[:len(files)-1])
			print("theme :", files)
		else :
			print("type :", files)
			files_remue = files.replace("_"," ")
			files_remue = filesremue.replace("-"," ")
			files_mot_list = decortique(files_remue)
			
			main = docx.createElement("type")
			skillN.appendChild(main)
			main.setAttribute("nom",files)

			text = docx.createTextNode("na")
			main.appendChild(text)

	with open(xmlcible, "w") as f:
	  	docx.writexml(f)


	return

class outils(object):
	#marqueur affichage id du theme en cours
	def etpuis(rien,skilla,enplus):
		print ( skilla )

	#cherche un element
	def chercher(rien,expertise,critere,option):
		resultat = False
		#variable globale pour version 0.x travaillant uniquement sur arbre1.xml
		for skillS in expertise :
			if skillS.getAttribute("id")==critere :
				print("ok")
				resultat = True
		return resultat

	#mets à jour le xml avec l'objet skilla
	def maj(rien,skilla,enplus):
		#ouvre le fichier xml pour maj
		chemin_encours = os.path.abspath(os.path.dirname(__file__))
		doca=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")
		#créé une branche avec le contenu
		id_encours = skilla.getAttribute("id")
		#mode verbeux
		print("f",id_encours)
		#possible
		possible=recherche_internet(id_encours,enplus)
		if not possible:
			element=str(enplus) #[2:len(enplus)-2]
			#element=element[2:len(element)-2]
			x = doc.createElement('type nom="'+element+'"')
			#genere la branche dans le squelette du xml
			skilla.appendChild(x)
			#maj le xml
			with open("arbre1.xml", "w") as f:
		 	  	doc.writexml(f)
		return

	#mets à jour le xml avec l'objet skilla
	def creer(rien,args,cible):
		#ouvre le fichier xml pour maj
		chemin_encours = os.path.abspath(os.path.dirname(__file__))
		
		#possible
		roota = doc.getElementsByTagName("classement")
		skill = roota[0]

		skillN = doc.createElement("theme")
		skill.appendChild(skillN)
		skillN.setAttribute("id",cible)

		main = doc.createElement("type")
		skillN.appendChild(main)
		main.setAttribute("nom",args)

		text = doc.createTextNode('nooption')
		main.appendChild(text)


		with open("arbre1.xml", "w") as f:
		  	doc.writexml(f)
		return skillN

	#deplace element vers id_encours
	def deplacer(rien,element,id_encours):
		commande= 'mv ' + element + ' ' + id_encours
		#print pour version test
		print( commande )
		return

#fonction principale.Scrute le fichier arbre1.xml. Rajoute tag si besoin dans le xml ou deaplce le fichier cible vers le repertoire associé au tag
def scrut(fonction="",arguments=""):
	#passe : l arguments (element_encours > copie de element > unicité de arguments) a deja ete trouve une fois

	#arguments : pour add et maj liste des tags a chercher
	#fonction appelée pour la scrutation
	#expertise: liste des balise theme dans schema xml = theme.id{type.nom}
	#skill: expertise unicité (dit niv1)
	#niv2 : liste des types d un theme (pris dans un unicité de skill) dans schema xml = theme.id{type.nom}
	#skill2 : niv2 unicité

	#id_encours : xml = theme.id{type.nom}
	#nom_encours : xml = theme.id{type.nom}
	#element : arguments unicité
	#element_encours : copie de l element unicité

	#node: api interne, boite outils methodes maj deplacer

	i=0
	mode_elargie = False
	mode_creer = False
	node=outils()

	rep_encours=os.path.dirname(__file__)
	if not mode_elargie:
		rep_encours = rep_encours.replace("/"," ")
		rep_encours_split = rep_encours.split()
		rep_encours = rep_encours_split[len(rep_encours_split) - 2]

	skill_passe=False

	#a partir du xml defini en variable globale on recupere la liste ces tag avec theme
	expertise = doc.getElementsByTagName("theme")

	#on parcours le squelette xml definit : theme > 'id > type > 'nom
	for skill in expertise:
		element_encours="e"
		passe=False
		#on recupere les paramètres
		id_encours = skill.getAttribute("id")
		niv2 = skill.getElementsByTagName("type")
		#pour chaque niveau du squelette xml
		# FONCTION tagp
		if fonction == "tagp" :
			if id_encours == arguments[0] :
				#mode verbeux
				print ("rajout : ", arguments[1], " dans ", arguments[0])
				node.maj(skill,arguments[1])
		for skill2 in niv2:
			nom_encours = skill2.getAttribute("nom")
			# FONCTION tagv
			if fonction == "tagv" :
				if id_encours == arguments[0] :
					print (nom_encours)
			else :
				#on gère chaque elements passés en argument pour un niveau
				for element in arguments:
					#sauvegarde pour suivi dans algo
					element_encours=element

					print("ne :" + id_encours + ", re :" + rep_encours )
					if id_encours == rep_encours and not passe:
						skill_nom_rep_encours = skill2
						skill_passe = True
					#si un des arguments est dans le niveau du xml en cours
					if nom_encours in element:
						#on note qu il a ete trouvé:flag a 1
						passe=True
						#compteur du nombres d elements trouvé
						i=i+1

						#mode verbeux
						print("c",element_encours,passe)
						#mise à 0 de la sauvegarde si trouve
						element_encours=""
						#prepa argument pour envoie vers fonction deplacé
						# FONCTION mv
						if fonction == "mv":
							node.deplacer(element,id_encours)
		passe=False

	#fichiers non tagable:rajout tag dans type en cours
	if not passe:
		#element_encours est le fichiers a indexer / skill est l arbre en cours d utilisation a incrementer
		#si tag fichier on maj
		# FONCTION add
		print("sp :" + rep_encours)
		

		if fonction == "add":
			#mise a jours lien tag
			if skill_passe :
				skill = skill_nom_rep_encours
				print("skill_passe")
			else:
			#choix du tag rep en cours ou non
				skill = skill

			#mode verbeux
			print("d",i,":",id_encours,":",nom_encours,":",arguments,":",passe)
			#if id_encours in element_encours:

			for sapiens in arguments:
				if skill_passe:
					node.maj(skill,sapiens)
				else if not mode_creer:
					skill = node.creer(sapiens, rep_encours)
					skill_passe = True
				else if mode_creer:
					skill = node.creer(sapiens, solo(sapiens))

			#mode verbeux
			print("gadd", ":",nom_encours,":",arguments)
			#mode verbeux
			print("e : ", arguments)
		# FONCTION imp
		if fonction == "imp":
			if element_encours[len(element_encours)-1] == ":" :
				rep_encours = element_encours.replace(":","")
			if len(rep_encours) == 0 :
				rep_encours = os.path.abspath(os.path.dirname(__file__))
			#mode verbeux
			print("d1 : ",i,":",id_encours,":",nom_encours,":",element_encours,":",passe)
			
			if not node.chercher(expertise, rep_encours, "option") :
				node.creer(element_encours,rep_encours)
			else :
				option = "option"


				#remise à 0 du flag pour recommencer boucle
				passe=False

		rep_encours=""
		#possibilite de partir sur autre  branche du xml
		niv2 = skill.getElementsByTagName("niv")

		for skill2 in niv2:
			bloc='bloc'
	skill_passe=False
	return

def main(arguments):
	#gestion arguments
	#description
	parser = argparse.ArgumentParser(description='gestion indexation fichiers')
	#backdoor
	#tous les arguments en paramètre
	parser.add_argument('pfiou', metavar='N', type=str, nargs='+', help='an integer for the accumulator')

	#gestion argument add:considere les fichiers en cours comme devant etre tag dans xml
	parser.add_argument("--add", help="index les tags des repertoires dans un xml existant")

	#gestion argument add:considere les fichiers en cours comme devant etre tag dans xml
	parser.add_argument("--gen", help="créé un nouvelle arbe dans un xml nouveau")

	#argument mv deaplce les fichiers du repertoire courant vers tag(si existe pas, induit un tag selon plusieurs critères dont repertoire en cours et nom du fichier en cours
	parser.add_argument("--mv", help="deplace selon l index")

	#argument 
	parser.add_argument("--tagv", help="consulter les tag d un repertoire")

	#argument 
	parser.add_argument("--tagp", help="rajouter un tag a un repetoire")

	#argument 
	parser.add_argument("--nouvt", help="genere un tag en fonction du contenu")

	#argument 
	parser.add_argument("--adddic", help="implemente le dictionnaire de mot par tag")


	args = parser.parse_args()
	#traitement des arguments
	if args.add:
		encours=args.pfiou
		scrut("add",encours)

	if args.gen:
		encours=args.pfiou
		gen("gen",encours)

	if args.mv:
		encours=args.pfiou
		scrut ("mv",encours)

	if args.tagv:
		encours=args.pfiou
		scrut ("tagv",encours)

	if args.tagp:
		encours=args.pfiou
		scrut ("tagp",encours)

	if args.nouvt:
		encours=args.pfiou
		creat ("nouvt",encours) 

	if args.adddic:
		encours=args.pfiou
		creat_def_manu ("addic",encours[0], encours[1:]) 

if __name__ == "__main__":
	main(sys.argv);

