from xml.dom.minidom import parse
dom = parse("arbre1.xml")
x = dom.createElement("foo")  # creates <foo />
dom.childNodes[0].appendChild(x)  # appends at end of 1st child's children
print (dom.toxml())

with open("arbre1.xml", "w") as f:
    dom.writexml(f)
