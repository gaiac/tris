#La version 0.x dans un premier temps a pour but de deplacer les fichiers present d'un repertoire source (non trié) vers un repertoire cible (tague dans le fichiers arbre1.xml) / dans un deuxième generé les tags quand un fichier du repertoire source n'est pas tague dans arbre1.xml a partir de critere divers comme le nom du fichier, son extension, son contenu / gestion de tag croise - optimisation de tris - tris semantique est l'objectif pour la version 0.9 / pour les version anterieur test de structure de donnees souples et mise en place de constructeur et fonctions metiers de base ( cherche, generer nouvea tag pour repertoire existant, generer repertoire si tag incompatible avec repertoire existant dans arbre1.xml )
#auteur LMG
#license GPL V3 : https://www.gaiac.eu/gpl.txt
#version: 0.3
#!/usr/bin/python3
from xml.dom.minidom import parse
import xml.dom.minidom
import argparse
import sys
import os


#variable globale pour version 0.x travaillant uniquement sur arbre1.xml
chemin_encours = os.path.abspath(os.path.dirname(__file__))
doc=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")

def recherche_internet(motclef1,motclef2):
	print("recherche effectuée")
	return False

class outils(object):
	#marqueur affichage id du theme en cours
	def etpuis(rien,skilla,enplus):
		print ( skilla )

	#cherche un element
	def chercher(rien,expertise,critere,option):
		resultat = False
		#variable globale pour version 0.x travaillant uniquement sur arbre1.xml
		for skillS in expertise :
			if skillS.getAttribute("id")==critere :
				print("ok")
				resultat = True
		return resultat

	#mets à jour le xml avec l'objet skilla
	def maj(rien,skilla,enplus):
		#ouvre le fichier xml pour maj
		chemin_encours = os.path.abspath(os.path.dirname(__file__))
		doca=xml.dom.minidom.parse(chemin_encours+"/arbre1.xml")
		#créé une branche avec le contenu
		id_encours = skilla.getAttribute("id")
		#mode verbeux
		print("f",id_encours)
		#possible
		possible=recherche_internet(id_encours,enplus)
		if not possible:
			x = doc.createElement('type nom="'+enplus+'"')
			#genere la branche dans le squelette du xml
			skilla.appendChild(x)
			#maj le xml
			with open("arbre1.xml", "w") as f:
		 	  	doc.writexml(f)
		return

	#mets à jour le xml avec l'objet skilla
	def creer(rien,skilla,args,cible):
		#ouvre le fichier xml pour maj
		chemin_encours = os.path.abspath(os.path.dirname(__file__))
		
		#créé une branche avec le contenu
		id_encours = skilla.getAttribute("id")
		#mode verbeux
		print("f",id_encours)
		#possible
		roota = doc.getElementsByTagName("classement")
		skill = roota[0]

		skillN = doc.createElement("theme")
		skill.appendChild(skillN)
		skillN.setAttribute("id",cible)

		main = doc.createElement("type")
		skillN.appendChild(main)
		main.setAttribute("nom",args)

		text = doc.createTextNode('nooption')
		main.appendChild(text)


		with open("arbre1.xml", "w") as f:
		  	doc.writexml(f)
		return

	#deplace element vers id_encours
	def deplacer(rien,element,id_encours):
		commande= 'mv ' + element + ' ' + id_encours
		#print pour version test
		print( commande )
		return

#fonction principale.Scrute le fichier arbre1.xml. Rajoute tag si besoin dans le xml ou deaplce le fichier cible vers le repertoire associé au tag
def scrut(fonction="",arguments=""):
	#passe : l arguments (element_encours > copie de element > unicité de arguments) a deja ete trouve une fois

	#arguments : pour add et maj liste des tags a chercher
	#fonction appelée pour la scrutation
	#expertise: liste des balise theme dans schema xml = theme.id{type.nom}
	#skill: expertise unicité (dit niv1)
	#niv2 : liste des types d un theme (pris dans un unicité de skill) dans schema xml = theme.id{type.nom}
	#skill2 : niv2 unicité

	#id_encours : xml = theme.id{type.nom}
	#nom_encours : xml = theme.id{type.nom}
	#element : arguments unicité
	#element_encours : copie de l element unicité

	#node: api interne, boite outils methodes maj deplacer

	i=0
	node=outils()
	rep_encours=""

	#a partir du xml defini en variable globale
	expertise = doc.getElementsByTagName("theme")

	#on parcours le squelette xml definit : theme > type > nom
	for skill in expertise:
		element_encours="e"
		passe=False
		#on recupere les paramètres
		id_encours = skill.getAttribute("id")
		niv2 = skill.getElementsByTagName("type")
		#pour chaque niveau du squelette xml
		# FONCTION tagp
		if fonction == "tagp" :
			if id_encours == arguments[0] :
				#mode verbeux
				print ("rajout : ", arguments[1], " dans ", arguments[0])
				node.maj(skill,arguments[1])
		for skill2 in niv2:
			nom_encours = skill2.getAttribute("nom")
			# FONCTION tagv
			if fonction == "tagv" :
				if id_encours == arguments[0] :
					print (nom_encours)
			else :
				#on gère chaque elements passés en argument pour un niveau
				for element in arguments:
					#sauvegarde pour suivi dans algo
					element_encours=element
					#si un des arguments est dans le niveau du xml en cours
					if nom_encours in element:
						#on note qu il a ete trouvé:flag a 1
						passe=True
						#compteur du nombres d elements trouvé
						i=i+1
						z=z+1
						#mode verbeux
						print("c",element_encours,passe)
						#mise à 0 de la sauvegarde si trouve
						element_encours=""
						#prepa argument pour envoie vers fonction deplacé
						# FONCTION mv
						if fonction == "mv":
							node.deplacer(element,id_encours)

					#fichiers non tagable:rajout tag dans type en cours
					if not passe:
						#element_encours est le fichiers a indexer / skill est l arbre en cours d utilisation a incrementer
						#si tag fichier on maj
						# FONCTION add
						if fonction == "add":
							#mode verbeux
							print("d",i,":",id_encours,":",nom_encours,":",element_encours,":",passe)
							if id_encours in element_encours:
								node.maj(skill,element_encours)
								#mode verbeux
								print("g", ":",nom_encours,":",element_encours)
							#mode verbeux
							print("e : ", element_encours)
						# FONCTION imp
						if fonction == "imp":
							if element_encours[len(element_encours)-1] == ":" :
								rep_encours = element_encours.replace(":","")
							if len(rep_encours) == 0 :
								rep_encours = os.path.abspath(os.path.dirname(__file__))
							#mode verbeux
							print("d1 : ",i,":",id_encours,":",nom_encours,":",element_encours,":",passe)
							
							if not node.chercher(expertise, rep_encours, "option") :
								node.creer(skill,element_encours,rep_encours)
							else :
								option = "option"
						passe=False


					#remise à 0 du flag pour recommencer boucle
					passe=False
				#suivis par branche du nombre de trouve:pour debuggage
				z=0
		rep_encours=""
		#possibilite de partir sur autre  branche du xml
		niv2 = skill.getElementsByTagName("niv")

		for skill2 in niv2:
			bloc='bloc'

def main(arguments):
	#gestion arguments
	#description
	parser = argparse.ArgumentParser(description='gestion indexation fichiers')
	#backdoor
	#tous les arguments en paramètre
	parser.add_argument('pfiou', metavar='N', type=str, nargs='+', help='an integer for the accumulator')

	#gestion argument add:considere les fichiers en cours comme devant etre tag dans xml
	parser.add_argument("--add", help="index les tags des repertoires")

	#argument mv deaplce les fichiers du repertoire courant vers tag(si existe pas, induit un tag selon plusieurs critères dont repertoire en cours et nom du fichier en cours
	parser.add_argument("--mv", help="deplace selon l index")

	#argument 
	parser.add_argument("--tagv", help="consulter les tag d un repertoire")

	#argument 
	parser.add_argument("--tagp", help="rajouter un tag a un repetoire")

	#argument 
	parser.add_argument("--imp", help="mettre a jours le xml avec les nouveaux fichiers et les nouveaux repertoire")

	args = parser.parse_args()
	#traitement des arguments
	if args.add:
		encours=args.pfiou
		scrut("add",encours)

	if args.mv:
		encours=args.pfiou
		scrut ("mv",encours)

	if args.tagv:
		encours=args.pfiou
		scrut ("tagv",encours)

	if args.tagp:
		encours=args.pfiou
		scrut ("tagp",encours)

	if args.imp:
		encours=args.pfiou
		scrut ("imp",encours)

if __name__ == "__main__":
	main(sys.argv);

