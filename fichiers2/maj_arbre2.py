from xml.dom.minidom import parse, Text

dom = parse("updates.xml")

updateSite = dom.getElementsByTagName('UpdateSite')[0]
text = Text()
text.data = 'test'
updateSite.appendChild(text)

with open("new_updates.xml", "wb") as f:
    dom.writexml(f)
